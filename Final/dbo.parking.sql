﻿CREATE TABLE [dbo].[parking] (
    [fId]   INT NOT NULL primary key IDENTITY(1,1),
    [停車場名稱]    NVARCHAR (MAX) NULL,
    [停車場地址]    NVARCHAR (MAX) NULL,
    [關閉]       BIT           DEFAULT ((0)) NOT NULL,
);