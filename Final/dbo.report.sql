﻿CREATE TABLE [dbo].[report] (
    [fId]   INT NOT NULL primary key IDENTITY(1,1),
    [停車場名稱] NVARCHAR (MAX) NOT NULL,
    [停車場地址] NVARCHAR (MAX) NOT NULL,
    [回饋內容]  NVARCHAR (MAX) NOT NULL,
);