﻿CREATE TABLE [dbo].[StockClose]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[stock_id] INT NOT NULL,
	[close_price] FLOAT NOT NULL
)
