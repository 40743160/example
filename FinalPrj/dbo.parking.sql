﻿CREATE TABLE [dbo].[parking] (
    [fId]      INT           NOT NULL,
    [停車場型態]    NVARCHAR (50) NULL,
    [停車場名稱]    NVARCHAR (50) NULL,
    [停車場地址]    NVARCHAR (50) NULL,
    [一般大型車位數]  INT           DEFAULT ((0)) NOT NULL,
    [一般小型車位數]  INT           DEFAULT ((0)) NOT NULL,
    [身障者小型車位數] INT           DEFAULT ((0)) NOT NULL,
    [婦幼者小型車位數] INT           DEFAULT ((0)) NOT NULL,
    [綠能小型車位數]  INT           DEFAULT ((0)) NOT NULL,
    [一般機車位數]   INT           DEFAULT ((0)) NOT NULL,
    [身障者機車位數]  INT           DEFAULT ((0)) NOT NULL,
    [收費時間]     NVARCHAR (21) NULL,
    [收費費率]     FLOAT (53)    DEFAULT ((0)) NOT NULL,
    [維修中]      BIT           DEFAULT ((0)) NOT NULL,
    [維修原因]     NVARCHAR (50) NULL,
    [關閉] BIT NOT NULL DEFAULT 0, 
    PRIMARY KEY CLUSTERED ([fId] ASC)
);

