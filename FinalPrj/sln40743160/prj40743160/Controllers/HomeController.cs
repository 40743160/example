﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using prj40743160.Models;

namespace prj40743160.Controllers
{
    public class HomeController : Controller
    {
        parkingEntities admin = new parkingEntities();
        parkingEntities1 parking = new parkingEntities1();
        reportEntities report = new reportEntities();

        private string[] xmlsrc = new string[3]{
            //臺南市公有免費停車場
            "https://citypark.tainan.gov.tw/App/parking.ashx?verCode=5177E3481D&type=1&ftype=4&exportTo=2",
            //臺南市公有收費停車場
            "https://citypark.tainan.gov.tw/App/parking.ashx?verCode=5177E3481D&type=2&ftype=4&exportTo=2",
            //臺南市民營停車場
            "https://citypark.tainan.gov.tw/App/parking.ashx?verCode=5177E3481D&type=3&ftype=4&exportTo=2"
        };

        MapDistanceServices mapDistanceServices = new MapDistanceServices();
        string MapUrl = "http://maps.google.com/maps/api/geocode/json?region=tw&language=zh-TW&sensor=false";

        private List<string> getsrcs(string _public)
        {
            List<string> srcs = new List<string>();
            if (_public == "不限")
            {
                return xmlsrc.ToList();
            }
            if (_public == "公有")
            {
                srcs.Add(xmlsrc[0]);
                srcs.Add(xmlsrc[1]);
            }
            else
            {
                srcs.Add(xmlsrc[2]);
            }
            return srcs;
        }

        public ActionResult Index(string 停車場型態 = "不限",
            int 一般大型車 = 0, int 一般小型車 = 0, int 身障者小型車 = 0,
            int 婦幼者小型車 = 0, int 綠能小型車 = 0, int 一般機車 = 0, int 身障者機車 = 0)
        {
            //string url = "https://citypark.tainan.gov.tw/App/parking.ashx?verCode=5177E3481D&type=1&ftype=4&exportTo=2";
            List<string> srcs = getsrcs(停車場型態);
            string s = "<table style='width: 80%; margin: 0 auto;' class='table table-striped'>" +
                "<thead><tr>" +
                "<th style='width: 40px'>#</th>" +
                "<th style='width: 200px'>停車場型態</th>" +
                "<th style='width: 200px'>停車場名稱</th>" +
                "<th style='width: 200px'>停車場地址</th>" +
                "<th style='width: 200px'>即時車位</th>" +
                "<th style='width: 200px'>一般大型車</th>" +
                "<th style='width: 200px'>一般小型車</th>" +
                "<th style='width: 200px'>身障者小型車</th>" +
                "<th style='width: 200px'>婦幼者小型車</th>" +
                "<th style='width: 200px'>綠能小型車</th>" +
                "<th style='width: 200px'>一般機車</th>" +
                "<th style='width: 200px'>身障者機車</th>" +
                "<th style='width: 200px'>收費時間</th>" +
                "<th style='width: 200px'>收費費率</th>" +
                "</thead></tr>";
            foreach (string src in srcs)
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(src);
                XmlNodeList parkingList = xmlDoc.DocumentElement.ChildNodes[0].ChildNodes;
                for (int i = 0; i < parkingList.Count; i++)
                {
                    if (int.Parse(parkingList[i]["一般大型車"].InnerText) < 一般大型車 ||
                        int.Parse(parkingList[i]["一般小型車"].InnerText) < 一般小型車 ||
                        int.Parse(parkingList[i]["身障者小型車"].InnerText) < 身障者小型車 ||
                        int.Parse(parkingList[i]["婦幼者小型車"].InnerText) < 婦幼者小型車 ||
                        int.Parse(parkingList[i]["綠能小型車"].InnerText) < 綠能小型車 ||
                        int.Parse(parkingList[i]["一般機車"].InnerText) < 一般機車 ||
                        int.Parse(parkingList[i]["身障者機車"].InnerText) < 身障者機車)
                        continue;
                    s += "<tr>" +
                         "<td scope='row'>" + (i + 1).ToString() + "</th>" +
                         "<td>" + parkingList[i]["停車場型態"].InnerText + "</td>" +
                         "<td><a href='https://www.google.com.tw/maps/search/" + parkingList[i]["停車場地址"].InnerText + "' target='_blank'> " + parkingList[i]["停車場名稱"].InnerText + "</a></td>" +
                         "<td>" + parkingList[i]["停車場地址"].InnerText + "</td>" +
                         "<td>" + (parkingList[i]["即時車位"].InnerText == "-1" ? "未提供" : parkingList[i]["即時車位"].InnerText) + "</td>" +
                         "<td>" + parkingList[i]["一般大型車"].InnerText + "</td>" +
                         "<td>" + parkingList[i]["一般小型車"].InnerText + "</td>" +
                         "<td>" + parkingList[i]["身障者小型車"].InnerText + "</td>" +
                         "<td>" + parkingList[i]["婦幼者小型車"].InnerText + "</td>" +
                         "<td>" + parkingList[i]["綠能小型車"].InnerText + "</td>" +
                         "<td>" + parkingList[i]["一般機車"].InnerText + "</td>" +
                         "<td>" + parkingList[i]["身障者機車"].InnerText + "</td>" +
                         "<td>" + (parkingList[i]["收費時間"].InnerText == "" ? "無標示" : parkingList[i]["收費時間"].InnerText) + "</td>" +
                         "<td>" + (parkingList[i]["收費費率"].InnerText == "" ? "無標示" : parkingList[i]["收費時間"].InnerText) + "</td>";

                    if (Session["isLogin"] == null)
                    {
                        s += "<td><a class='btn btn-primary' href='Home/Report?停車場名稱=" + parkingList[i]["停車場名稱"].InnerText + "&停車場地址=" + parkingList[i]["停車場地址"].InnerText + "'>回饋與反應</a></td>";
                    }

                    s += "</tr>";
                }
            }
            ViewBag.Content = s;
            return View();
        }

        /// <summary>
        /// 登入介面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string 帳號, string 密碼)
        {
            //驗證帳號密碼
            if (admin.admins
                .Where(ad => ad.帳號 == 帳號 && ad.密碼 == 密碼)
                .FirstOrDefault() != null)
            {
                ViewData.Clear();
                Session["isLogin"] = 1;
                return View("Index");
            }
            else
            {
                ViewData["Msg"] = "登入失敗...";
                return View();
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Clear();
            return View("Index");
        }

        [HttpGet]
        public ActionResult Report(string 停車場名稱, string 停車場地址)
        {
            if (Session["isLogin"] != null && ((int)Session["isLogin"]) == 1)
            {
                var reports = report.report.OrderBy(r => r.fId).ToList();
                string s = "<table class='table table-striped'><thead><tr><th width='40px'>#</th><th width='200px'>停車場名稱</th><th width='200px'>停車場地址</th><th width='200px'>回饋內容</th></tr></thead>";
                foreach (var r in reports)
                {
                    s += "<tr>" +
                         "<td>" + r.fId.ToString() + "</th>" +
                         "<td><a href='ReportDetails?fId=" + r.fId.ToString() + "'>" + r.停車場名稱 + "</a></td>" +
                         "<td>" + r.停車場地址 + "</td>" +
                         "<td>" + (r.回饋內容.Length > 10 ? r.回饋內容.Substring(0, 10) : r.回饋內容) + "</td>" +
                         "</tr>";
                }
                ViewData["report_list"] = s + "</table>";
            }
            else
            {
                if (停車場名稱 == null || 停車場地址 == null)
                {
                    return Redirect("Index");
                }
                ViewData["停車場名稱"] = 停車場名稱;
                ViewData["停車場地址"] = 停車場地址;
            }
            return View();
        }

        [HttpPost]
        public ActionResult Report(report _report)
        {
            report.report.Add(_report);
            report.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ReportDetails(int fId)
        {
            return View(report.report.Where(m => m.fId == fId).FirstOrDefault());
        }

        //以下用於Google API

        public string getdis(double lat1, double lng1, double lat2, double lng2)
        {
            return mapDistanceServices.GetDistance(lat1, lng1, lat2, lng2).ToString();
        }


        public class MapDistanceServices
        {
            public MapDistanceServices() { }

            private const double EARTH_RADIUS = 6378.137;
            private double rad(double d)
            {
                return d * Math.PI / 180.0;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="lat1">緯度1</param>
            /// <param name="lng1">經度1</param>
            /// <param name="lat2">緯度2</param>
            /// <param name="lng2">經度2</param>
            /// <returns></returns>
            public double GetDistance(double lat1, double lng1, double lat2, double lng2)
            {
                double dblResult = 0;
                double radLat1 = rad(lat1);
                double radLat2 = rad(lat2);
                double distLat = radLat1 - radLat2;
                double distLng = rad(lng1) - rad(lng2);
                dblResult = 2 * Math.Asin(Math.Sqrt(Math.Pow(Math.Sin(distLat / 2), 2) +
                                Math.Cos(radLat1) * Math.Cos(radLat2) * Math.Pow(Math.Sin(distLng / 2), 2)));
                dblResult = dblResult * EARTH_RADIUS;
                //dblResult = Math.Round(dblResult * 10000) /10000;  //這回傳變成公里,少3個0變公尺
                dblResult = Math.Round(dblResult * 10000) / 10;

                return dblResult;
            }
        }
    }
}