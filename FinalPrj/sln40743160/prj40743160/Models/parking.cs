//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace prj40743160.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class parking
    {
        public int fId { get; set; }
        public string 停車場型態 { get; set; }
        public string 停車場名稱 { get; set; }
        public string 停車場地址 { get; set; }
        public int 一般大型車位數 { get; set; }
        public int 一般小型車位數 { get; set; }
        public int 身障者小型車位數 { get; set; }
        public int 婦幼者小型車位數 { get; set; }
        public int 綠能小型車位數 { get; set; }
        public int 一般機車位數 { get; set; }
        public int 身障者機車位數 { get; set; }
        public string 收費時間 { get; set; }
        public double 收費費率 { get; set; }
        public bool 維修中 { get; set; }
        public string 維修原因 { get; set; }
    }
}
