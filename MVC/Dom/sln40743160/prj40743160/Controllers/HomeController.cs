﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using prj40743160.Controllers;

namespace prj40743160.Controllers
{
    public class HomeController : Controller
    {
        Stock stock;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult showGraph() {
            stock.StockUpdate("");
            return View();
        }
    }
}