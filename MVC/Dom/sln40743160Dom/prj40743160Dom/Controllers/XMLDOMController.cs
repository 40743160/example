﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace prj40743160Dom.Controllers
{
    public class XMLDOMController : Controller
    {
        // GET: XMLDOM
        public ActionResult Index()
        {
            return View();
        }

        public string Example4()
        {
            string show = "";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("http://140.130.34.14/dom/javascript/book2.xml");
            XmlNodeList fnodeList = xmlDoc.DocumentElement.ChildNodes;
            //取出根標籤(Document element)(bookstore)下的第一層子標籤(book)
            show += "<hr /><h2>資料顯示</h2><hr />";
            for (int i = 0; i < fnodeList.Count; i++)
            {
                if (fnodeList[i].NodeType.ToString() == "Element")  //Process only element nodes (type 1 是Element)         
                    show += fnodeList[i].Name + "<br />";
            }
            show += "<hr /> 課程:Web技術與應用";
            return show;
        }

        public String DOMHW01()
        {
            string show = "";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("http://140.130.34.14/dom/javascript/book2.xml");
            XmlNodeList fnodeList = xmlDoc.DocumentElement.ChildNodes;
            //取出根標籤(Document element)(bookstore)下的第一層子標籤(book)
            show += "<hr /><h2>資料顯示</h2><hr />";
            for (int i = 0; i < fnodeList.Count; i++)
            {
                if (fnodeList[i].NodeType.ToString() == "Element") {   //Process only element nodes (type 1 是Element)         
                    show += fnodeList[i].Name + "<br />";
                    XmlNodeList snodeList = fnodeList[i].ChildNodes;
                    for (int j = 0; j < snodeList.Count; j++)
                    {
                        if (snodeList[j].NodeType.ToString() == "Element")
                        {
                            show += snodeList[j].Name + "：" + snodeList[j].InnerText + "<br/>";
                        }
                    }

                }
            }
            show += "<hr /> 課程:Web技術與應用";
            return show;
        }

        public String loadDoc(String xmlFile) {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xmlFile);
            XmlNodeList fnodeList = xmlDoc.DocumentElement.ChildNodes;
            String html = "";
            html += "<h2>書名表格顯示</h2><hr/><table border='1'><thead><tr><th>書號</th><th>書名</th><th>作者</th><th>價錢</th><th>是否有存貨</th></tr></thead><tbody>";
            for (int i = 0; i < fnodeList.Count; i++)
            {    //讀取 book 子節點
                html += "<tr>";
                XmlNodeList snodeList = fnodeList[i].ChildNodes;

                for (int j = 0; j < snodeList.Count; j++)
                { // 顯示所有子節點
                    html += "<td>" + snodeList[j].InnerText + "</td>";
                }
                html += "<td>" + fnodeList[i].Attributes["sales"].Value + "</td><tr/>";
            }
            html += "</tbody></table><hr/>";
            return html;
        }

        public String DOMHW02() {
            return loadDoc("http://140.130.34.14/dom/javascript/book4.xml") 
                + loadDoc("http://140.130.34.14/dom/javascript/book5.xml");
        }

        public String DOMHW08() {
            //url: https://udn.com/rssfeed/news/2/6638?ch=news
            string show = "";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("https://udn.com/rssfeed/news/2/6638?ch=news");
            XmlNodeList fnodeList = xmlDoc.DocumentElement.ChildNodes;
            //取出根標籤(Document element)(bookstore)下的第一層子標籤(book)
            show += "<hr /><h2>資料顯示</h2><hr />";
            for (int i = 0; i < fnodeList.Count; i++)
            {
                if (fnodeList[i].NodeType.ToString() == "Element")
                {   //Process only element nodes (type 1 是Element)         
                    show += fnodeList[i].Name + "<br />";
                    XmlNodeList snodeList = fnodeList[i].ChildNodes;
                    for (int j = 0; j < snodeList.Count; j++)
                    {
                        if (snodeList[j].NodeType.ToString() == "Element")
                        {
                            show += snodeList[j].Name + "：" + snodeList[j].InnerText + "<br/>";
                        }
                    }

                }
            }
            show += "<hr /> 課程:Web技術與應用";
            return show;
        }
    }
}