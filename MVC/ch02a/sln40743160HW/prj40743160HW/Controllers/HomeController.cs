﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using prj40743160HW.Models;

namespace prj40743160HW.Controllers
{
    public class HomeController : Controller
    {
        private Entities db = new Entities();

        // GET: C40743160user
        public ActionResult Index()
        {
            return View(db.C40743160user.ToList());
        }

        // GET: C40743160user/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            C40743160user c40743160user = db.C40743160user.Find(id);
            if (c40743160user == null)
            {
                return HttpNotFound();
            }
            return View(c40743160user);
        }

        // GET: C40743160user/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: C40743160user/Create
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "fid,fAccount,fPassword,fEmail")] C40743160user c40743160user)
        {
            c40743160user.fDate = DateTime.UtcNow;
            if (ModelState.IsValid)
            {
                db.C40743160user.Add(c40743160user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(c40743160user);
        }

        // GET: C40743160user/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            C40743160user c40743160user = db.C40743160user.Find(id);
            if (c40743160user == null)
            {
                return HttpNotFound();
            }
            return View(c40743160user);
        }

        // POST: C40743160user/Edit/5
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "fid,fAccount,fPassword,fEmail")] C40743160user c40743160user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(c40743160user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(c40743160user);
        }

        // GET: C40743160user/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            C40743160user c40743160user = db.C40743160user.Find(id);
            if (c40743160user == null)
            {
                return HttpNotFound();
            }
            return View(c40743160user);
        }

        // POST: C40743160user/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            C40743160user c40743160user = db.C40743160user.Find(id);
            db.C40743160user.Remove(c40743160user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
