﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using prj40743160.Models;

namespace prj40743160.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            List<NightMarket> list = new List<NightMarket>() {
                new NightMarket(){ Id="A01", Nmae="夜市01", Address="地址01" },
                new NightMarket(){ Id="A02", Nmae="夜市02", Address="地址02" },
                new NightMarket(){ Id="A03", Nmae="夜市03", Address="地址03" },
                new NightMarket(){ Id="A04", Nmae="夜市04", Address="地址04" },
                new NightMarket(){ Id="A05", Nmae="夜市05", Address="地址05" },
            };
            return View(list);
        }
    }
}