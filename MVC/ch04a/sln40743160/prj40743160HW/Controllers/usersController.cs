﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using prj40743160HW.Models;

namespace prj40743160HW.Controllers
{
    public class usersController : Controller
    {
        private DB40743160Entities db = new DB40743160Entities();

        // GET: users
        public ActionResult Index()
        {
            if (Session["user"] != null)
            {
                if ((Session["user"] as user).fAdmin == true)
                {
                    Session["users"] = db.user.ToList();
                }
                return View();
            }
            return View();
        }

        /// <summary>
        /// 登入介面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// 登入 API
        /// </summary>
        /// <param name="fAccount"></param>
        /// <param name="fPassword"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login(string fAccount, string fPassword)
        {
            user user = db.user
                .Where(a => a.fAccount == fAccount && a.fPassword == fPassword)
                .FirstOrDefault();

            if (user != null)
            {
                Session["user"] = user;
                Session["userID"] = user.fId;
                ViewBag.Msg = "登入成功 " + fAccount;
                if ((Session["user"] as user).fAdmin == true)
                {
                    Session["users"] = db.user.ToList();
                }
                else {
                    Session["users"] = null;
                }
                return View("Index");
            }
            else
            {
                ViewBag.Msg = "登入失敗";
                return View();
            }
        }

        [HttpGet]
        public ActionResult Logout() {
            Session.Clear();
            return View("index");
        }

        // GET: users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.user.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        /// <summary>
        /// 註冊介面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: users/Create
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "fAccount,fPassword,fEmail,fDate")] user user)
        {
            if (ModelState.IsValid)
            {
                db.user.Add(user);
                db.SaveChanges();
                ViewBag.Msg = "註冊資料如下：<br>" +
                    "帳號：" + user.fAccount + "<br>" +
                    "密碼：" + user.fPassword + "<br>" +
                    "信箱：" + user.fEmail + "<br>" +
                    "生日：" + user.fDate;
                Session["user"] = user;
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.user.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: users/Edit/5
        // 若要免於過量張貼攻擊，請啟用想要繫結的特定屬性，如需
        // 詳細資訊，請參閱 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "fId,fAccount,fPassword,fEmail")] user user, HttpPostedFileBase[] photo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                for (int i = 0; i < photo.Length; i++)
                {
                    HttpPostedFileBase f = photo[i];
                    if (f != null)
                    {
                        f.SaveAs(Path.Combine(Server.MapPath("~/images"), user.fId + Path.GetExtension(f.FileName)));
                    }
                }
                ViewBag.Msg = "編輯成功";
                return RedirectToAction("Index", user);
            }
            ViewBag.Msg = "錯誤資料";
            return View(user);
        }

        // GET: users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.user.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            user user = db.user.Find(id);
            db.user.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
