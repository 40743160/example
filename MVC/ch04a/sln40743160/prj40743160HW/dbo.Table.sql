﻿CREATE TABLE [dbo].[user]
(
    [fId] int  NOT NULL,
    [fAccount] nvarchar(50)  NOT NULL,
    [fPassword] nvarchar(50)  NOT NULL,
    [fDate] datetime  NULL,
    [fAdmin] bit  NULL,
    [fEmail] nvarchar(50)  NULL
)
