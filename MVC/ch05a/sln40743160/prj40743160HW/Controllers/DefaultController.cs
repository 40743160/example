﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using prj40743160HW.Models;

namespace prj40743160HW.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }

        // 查詢所有員工記錄
        public string ShowEmployee()
        {
            NorthwindEntities db = new NorthwindEntities();
            //Linq擴充方法寫法
            var result = db.員工;
            //var result = from m in db.員工
            //             select m;
            string show = "";
            foreach (var m in result)
            {
                show += "編號：" + m.員工編號 + "<br />";
                show += "姓名：" + m.姓名 + m.稱呼 + "<br />";
                show += "職稱：" + m.職稱 + "<hr>";
            }
            return show;
        }

        public string ShowEmployee1()
        {
            NorthwindEntities db = new NorthwindEntities();
            //Linq擴充方法寫法
            var result = db.員工;
            //var result = from m in db.員工
            //             select m;
            string show = "<table border='1'>" +
                "           <thead>" +
                "             <tr>" +
                "               <td>編號</td>" +
                "               <td>姓名</td>" +
                "               <td>職稱</td>" +
                "               <td>生日</td>" +
                "               <td>地址</td>" +
                "             </tr></thead><tbody>";
            int index = 0;
            foreach (var m in result)
            {
                index++;
                show += "<tr><td>" + m.員工編號 + "</td>" +
                    "        <td>" + m.姓名 + "</td>" +
                    "        <td>" + m.職稱 + "</td>" +
                    "        <td>" + m.出生日期 + "</td>" +
                    "        <td>" + m.地址 + "</td><tr/>";
            }
            show += "</tbody></table>";
            return show;
        }

        public ActionResult ShowEmployee2()
        {
            NorthwindEntities db = new NorthwindEntities();
            //Linq擴充方法寫法
            var result = db.員工;
            //var result = from m in db.員工
            //             select m;
            string show = "<table class='table table-hover'>" +
                "           <thead>" +
                "             <tr>" +
                "               <td>編號</td>" +
                "               <td>姓名</td>" +
                "               <td>職稱</td>" +
                "               <td>生日</td>" +
                "               <td>地址</td>" +
                "             </tr></thead><tbody>";
            int index = 0;
            foreach (var m in result)
            {
                index++;
                show += "<tr><td>" + m.員工編號 + "</td>" +
                    "        <td>" + m.姓名 + "</td>" +
                    "        <td>" + m.職稱 + "</td>" +
                    "        <td>" + m.出生日期 + "</td>" +
                    "        <td>" + m.地址 + "</td><tr/>";
            }
            show += "</tbody></table>";
            ViewBag.show = show;
            return View();
        }

        public string ShowCustomerByAddress(string keyword)
        {
            NorthwindEntities db = new NorthwindEntities();
            var result = db.客戶.Where(m => m.地址.Contains(keyword));
            string show = "";
            if (result == null) return "查無地址";
            foreach (var m in result)
            {
                show += "公司：" + m.公司名稱 + "<br />";
                show += "姓名：" + m.連絡人 + m.連絡人職稱 + "<br />";
                show += "地址：" + m.地址 + "<hr>";
            }
            return show;
        }

        public ActionResult QueryProvider()
        {
            return View();
        }

        [HttpPost]
        public ActionResult QueryProvider(string city)
        {
            NorthwindEntities db = new NorthwindEntities();
            //Linq擴充方法寫法
            var result = db.供應商
                .Where(m => m.地址.Contains(city))
                .OrderBy(m => m.電話);
            //var result = from m in db.員工
            //             select m;
            string show = "";
            foreach (var m in result)
            {
                show += "供應商：" + m.供應商1 + "<br />";
                show += "連絡人：" + m.連絡人 + "<br />";
                show += "電話：" + m.電話 + "<br/>";
                show += "地址：" + m.地址 + "<hr>";
            }
            ViewBag.show = show;
            return View();
        }
    }

}