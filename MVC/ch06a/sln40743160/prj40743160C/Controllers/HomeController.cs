﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace prj40743160C.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            DataTable dt = querySql("SELECT * FROM tStudent");
            return View(dt);
        }
        // GET: Home/Create
        public ActionResult Create()
        {
            return View();
        }
        // POST: Home/Create
        [HttpPost]
        public ActionResult Create(string fStuId, string fName, string fEmail, int fScore)
        {
            string sql = "INSERT INTO tStudent(fStuId,fName,fEmail,fScore)VALUES('"
                + fStuId.Replace("'", "''") + "',N'"
                + fName.Replace("'", "''") + "','"
                + fEmail.Replace("'", "''") + "',"
                + fScore + ")";
            executeSql(sql);
            return RedirectToAction("Index");
        }
        // GET: Home/Delete/id
        public ActionResult Delete(string id)
        {
            string sql = "DELETE FROM tStudent WHERE fStuId='"
                + id.Replace("'", "''") + "'";
            executeSql(sql);
            DataTable dt = querySql("SELECT * FROM tStudent");
            return RedirectToAction("Index");
        }

        // constr連接字串指定連接dbStudent.mdf資料庫
        string constr = @"Data Source=(LocalDB)\MSSQLLocalDB;" +
            "AttachDbFilename=|DataDirectory|dbStudent.mdf;" +
            "Integrated Security=True";

        //executeSql方法可傳入SQL字串來輯編資料表
        private void executeSql(string sql)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = constr;
            con.Open();
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        //querySql方法可傳入SQL字串並傳回DataTable物件
        private DataTable querySql(string sql)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = constr;
            SqlDataAdapter adp = new SqlDataAdapter(sql, con);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            return ds.Tables[0];
        }

    }
}