﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PagedList;
using prj40743160P.Models;

namespace prj40743160P.Controllers
{
    public class HomeController : Controller
    {
        NorthwindEntities2 db = new NorthwindEntities2();

        int pageSize = 10;

        public ActionResult Index(int page = 1)
        {
            var products = db.產品資料.OrderBy(m => m.產品編號).ToList();
            return View(products.ToPagedList(
                (page < 1 ? 1 : page),
                pageSize));
        }

        public ActionResult Create() {
            return View();
        }

        [HttpPost]
        public ActionResult Create(產品資料 產品資料) {
            db.產品資料.Add(產品資料);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int 產品編號)
        {
            var emp = db.產品資料.Where(m => m.產品編號 == 產品編號).FirstOrDefault();
            db.產品資料.Remove(emp);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int 產品編號)
        {
            return View(db.產品資料.Where(m => m.產品編號 == 產品編號).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult Edit(int 產品編號, string 產品, short 庫存量,
            int 類別編號, string 單位數量, int 單價, short 已訂購量, short 安全存量, bool 不再銷售)
        {
            var item = db.產品資料.Where(m => m.產品編號 == 產品編號).FirstOrDefault();
            item.產品 = 產品;
            item.庫存量 = 庫存量;
            item.類別編號 = 類別編號;
            item.單位數量 = 單位數量;
            item.單價 = 單價;
            item.已訂購量 = 已訂購量;
            item.安全存量 = 安全存量;
            item.不再銷售 = 不再銷售;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Details(int 產品編號) {
            return View(db.產品資料.Where(m => m.產品編號 == 產品編號).FirstOrDefault());
        }
    }
}