
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/28/2020 02:04:50
-- Generated from EDMX file: C:\Users\Quareta\Desktop\example\MVC\ch07a\sln40743160\prj40743160P\Models\NorthwindModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Northwind];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[產品資料]', 'U') IS NOT NULL
    DROP TABLE [dbo].[產品資料];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table '產品資料'
CREATE TABLE [dbo].[產品資料] (
    [產品編號] int IDENTITY(1,1) NOT NULL,
    [產品] nvarchar(40)  NOT NULL,
    [供應商編號] int  NULL,
    [類別編號] int  NULL,
    [單位數量] nvarchar(20)  NULL,
    [單價] decimal(19,4)  NULL,
    [庫存量] smallint  NULL,
    [已訂購量] smallint  NULL,
    [安全存量] smallint  NULL,
    [不再銷售] bit  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [產品編號] in table '產品資料'
ALTER TABLE [dbo].[產品資料]
ADD CONSTRAINT [PK_產品資料]
    PRIMARY KEY CLUSTERED ([產品編號] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------