﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace prj40743160.Controllers
{
    public class XMLController : Controller
    {
        // GET: XML
        public ActionResult Index()
        {
            string show = "<h2>農村地方美食小吃特色料理</h2><table class='table' id='tableshow'><tr><th>ID</th><th>名稱</th><th>地址</th><th>電話</th><th>說明</th><th>照片</th><th></th></tr>";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("http://localhost:56127/ODwsvTravelFood.xml");
            XmlNodeList fnodeList = xmlDoc.DocumentElement.ChildNodes;
            for (int i = 0; i < fnodeList.Count; i++) {
                if (fnodeList[i].HasChildNodes) {
                    show += "<tr>";
                    XmlNodeList snodeList = fnodeList[i].ChildNodes;

                    for (int j = 0; j < 5; j++) {
                        show += "<td>" + snodeList[j].InnerText + "</td>";
                    }
                    show += "<td><img src='" + snodeList[22].InnerText + "' style='width:200px;'></td>" +
                        "<td><a href='https://www.google.com.tw?maps/place/" + snodeList[2].InnerText + "' target='_blank' class='btn btn-info'>地圖</a></tr>";
                }
            }
            show += "</table>";
            ViewBag.Msg = show;
            return View();
        }
    }
}